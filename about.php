<?php

$ThemeInfo['eleven'] = array(
  'Name'        => 'Eleven',
  'Description' => "Eleven Project forum theme, based on Bootstrap for Vanilla (https://github.com/kasperisager/vanilla-bootstrap)",
  'Version'     => '',
  'Url'         => '',
  'Author'      => 'Eleven Team',
  'AuthorEmail' => '',
  'AuthorUrl'   => 'http://elevengiants.com',
  'License'     => 'Original vanilla-bootstrap theme is MIT licensed',
  'RequiredApplications' => array('Vanilla' => '2.1.x'),

  'Options' => array(
    'Styles' => array(
      'Default'   => '%s_default',
      'Bootstrap' => '%s_bootstrap',
      'Amelia'    => '%s_amelia',
      'Cerulean'  => '%s_cerulean',
      'Cosmo'     => '%s_cosmo',
      'Cyborg'    => '%s_cyborg',
      'Darkly'    => '%s_darkly',
      'Flatly'    => '%s_flatly',
      'Journal'   => '%s_journal',
      'Lumen'     => '%s_lumen',
      'Paper'     => '%s_paper',
      'Readable'  => '%s_readable',
      'Sandstone' => '%s_sandstone',
      'Simplex'   => '%s_simplex',
      'Slate'     => '%s_slate',
      'Spacelab'  => '%s_spacelab',
      'Superhero' => '%s_superhero',
      'United'    => '%s_united',
      'Yeti'      => '%s_yeti'
    )
  )
);
